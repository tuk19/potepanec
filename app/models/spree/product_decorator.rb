module Spree::ProductDecorator
  def related_products(display_num)
    Spree::Product.joins(:taxons).
      includes(master: [:images, :default_price]).
      where(spree_products_taxons: { taxon_id: taxons }).
      where.not(id: id).distinct.limit(display_num)
  end

  Spree::Product.prepend self
end
