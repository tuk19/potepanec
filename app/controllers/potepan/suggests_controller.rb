class Potepan::SuggestsController < ApplicationController
  def search
    if params[:keyword].blank?
      return render status: 400, json: { status: 400, message: "キーワードを入力してください (missing keyword)" }
    end

    response = API::Suggest.suggest(params[:keyword], params[:max_num])

    if response.status == 200
      render json: JSON.parse(response.body)
    else
      render status: response.status, json: { status: response.status, message: "#{response.status}エラーが発生しました (Error No.#{response.status} has occurred)" }
    end
  end
end
