require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe '#related_products' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product1) { create(:product, taxons: [taxon], name: "RUBY ON RAILS TOTE") }
    let!(:related_product2) { create(:product, taxons: [taxon], name: "RUBY ON RAILS BAG") }
    let!(:related_product3) { create(:product, taxons: [taxon], name: "RUBY ON RAILS BACKPACK") }
    let!(:related_product4) { create(:product, taxons: [taxon], name: "RUBY ON RAILS HANDBAG") }
    let!(:related_product5) { create(:product, taxons: [taxon], name: "RUBY ON RAILS CASE") }
    let!(:another_product) { create(:product) }
    let!(:related_product_num) { Settings.decorator.related_product_num }

    example '商品自身は取得しないこと' do
      expect(product.related_products(related_product_num)).not_to include(product)
    end

    example "関連商品以外は取得しないこと" do
      expect(product.related_products(related_product_num)).not_to include(another_product)
    end

    example "関連商品を重複せずに取得すること" do
      expect(product.related_products(related_product_num)).to match_array(
        [related_product1, related_product2, related_product3, related_product4]
      )
    end

    example "関連商品は4つまで取得すること" do
      expect(product.related_products(related_product_num)).not_to include(related_product5)
    end
  end
end
