require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title(page_title)" do
    subject { full_title(str) }

    context "引数が空文字の時" do
      let(:str) { "" }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "引数がnilの時" do
      let(:str) { nil }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "引数が文字列の時" do
      let(:str) { "RUBY ON RAILS BAG" }

      it { is_expected.to eq "RUBY ON RAILS BAG - BIGBAG Store" }
    end
  end
end
