require "rails_helper"
require "webmock/rspec"

RSpec.describe "Potepan::Suggests", type: :request do
  describe "GET #suggest" do
    let(:url) { Settings.suggest.url }
    let(:api_key) { Settings.suggest.api_key }

    before do
      WebMock.enable!
      WebMock.stub_request(:get, url).with(
        headers: header,
        query: { keyword: keyword, max_num: 5 }
      ).to_return(
        body: return_body,
        status: return_status
      )
      get potepan_suggests_path params: { keyword: keyword, max_num: 5 }
    end

    context "レスポンスが正常な場合" do
      let(:header) { { 'Authorization' => "Bearer #{api_key}", "Content-Type" => "application/json" } }
      let(:keyword) { "ruby" }
      let(:return_body) { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json }
      let(:return_status) { 200 }

      example "200ステータスが返ってくること" do
        expect(response.status).to eq 200
      end

      example "期待したbodyが返ってくること" do
        expect(response.body).to eq ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json
      end
    end

    context "keywordがblankの場合" do
      let(:header) { { 'Authorization' => "Bearer #{api_key}", "Content-Type" => "application/json" } }
      let(:keyword) { " " }
      let(:return_body) { ["ruby1", "ruby2", "ruby3", "ruby4", "ruby5"].to_json }
      let(:return_status) { 400 }

      example "400ステータスが返ってくること" do
        expect(response.status).to eq 400
      end
    end

    context "レスポンスが正常でない場合" do
      let(:header) { { "Content-Type" => "application/json" } }
      let(:keyword) { "ruby" }
      let(:return_body) { "Error" }
      let(:return_status) { 500 }

      example "500ステータスが返ってくること" do
        expect(response.status).to eq 500
      end
    end
  end
end
