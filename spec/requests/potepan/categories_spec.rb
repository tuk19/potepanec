require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    example "showページへアクセスできること" do
      expect(response).to have_http_status(:success)
    end

    example "商品名が含まれていること" do
      expect(response.body).to include(product.name)
    end

    example "商品価格が含まれていること" do
      expect(response.body).to include(product.display_price.to_s)
    end

    example "画像が表示されること" do
      product.images.each do |image|
        expect(response.body).to include(image.attachment(:large))
      end
    end
  end
end
