require "rails_helper"

RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:image) { create(:image) }
    let(:variant) { create(:variant, images: [image]) }
    let!(:related_product) { create(:product, taxons: [taxon], master: variant) }

    before do
      get potepan_product_path(product.id)
    end

    example "showページへアクセスできること" do
      expect(response).to have_http_status(:success)
    end

    example "商品名が含まれていること" do
      expect(response.body).to include(product.name)
    end

    example "商品価格が含まれていること" do
      expect(response.body).to include(product.display_price.to_s)
    end

    example "商品説明が含まれていること" do
      expect(response.body).to include(product.description)
    end

    example "リンクが含まれていること" do
      expect(response.body).to include('一覧ページへ戻る')
    end

    example "showページのレンダリングテスト" do
      expect(response).to render_template :show
    end

    example "関連商品が含まれていること" do
      expect(response.body).to include(related_product.name)
    end

    example "関連商品画像が含まれること" do
      expect(response.body).to include(
        "spree/products/#{related_product.images.first.id}/small" \
        "/#{related_product.images.first.attachment_file_name}"
      )
    end
  end
end
