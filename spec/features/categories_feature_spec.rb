require 'rails_helper'

RSpec.feature 'Categories_Features', type: :feature do
  describe "link_to_categories" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, name: "Mugs", taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:product) { create(:product, name: "RUBY ON RAILS MUG", taxons: [taxon]) }
    let!(:another_product) { create(:product) }

    background do
      visit potepan_category_path(taxon.id)
    end

    scenario '商品カテゴリー欄からカテゴリーページに遷移できるか' do
      within('ul.side-nav') do
        expect(page).to have_content(taxonomy.name)
        expect(page).to have_content(taxon.name)
        expect(page).to have_content(taxon.products.length)
        click_link taxon.name
      end
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(taxon.name)
      expect(page).to have_content(product.name)
      expect(page).to have_no_content(product.description)
    end

    scenario 'taxonに紐づかない商品が表示されない' do
      expect(page).to have_no_content(another_product.name)
    end

    scenario '商品ページへ遷移できるか' do
      click_link product.name
      expect(page).to have_content(product.description)
    end
  end
end
