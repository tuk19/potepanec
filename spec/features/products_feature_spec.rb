require 'rails_helper'

RSpec.feature 'Products_Features', type: :feature do
  describe "link_to_categories" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon], name: "RUBY ON RAILS TOTE") }
    let!(:another_product) { create(:product) }

    background do
      visit potepan_product_path(product.id)
    end

    scenario 'カテゴリーページへ遷移できるか' do
      click_link "一覧ページへ戻る"
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
    end

    scenario '関連商品へ遷移できるか' do
      within('div.productsContent') do
        expect(page).to have_content(related_product.name)
        expect(page).to have_content(related_product.display_price)
        expect(page).to have_no_content(another_product.name)
        click_link related_product.name
      end
      expect(current_url).to include(potepan_product_path(related_product.id))
      expect(page).to have_content(related_product.name)
      expect(page).to have_content(related_product.display_price)
      expect(page).to have_content(related_product.description)
      expect(page).to have_content(product.name)
      expect(page).to have_no_content(another_product.name)
    end
  end
end
