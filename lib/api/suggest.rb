require 'httpclient'

module API
  class Suggest
    def self.suggest(keyword, max_num)
      url = Settings.suggest.url
      header = { Authorization: "Bearer #{Settings.suggest.api_key}", "Content-Type": "application/json" }
      query = { keyword: keyword, max_num: max_num }
      client = HTTPClient.new
      client.get(url, header: header, query: query)
    end
  end
end
